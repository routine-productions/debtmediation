<?php
defined('_JEXEC') or die;
JHtml::_('behavior.framework', true);

$app			= JFactory::getApplication();
$doc			= JFactory::getDocument();
$itemid = JRequest::getVar('Itemid');

// get params
$templateparams	= $app->getTemplate(true)->params;

// add styles
$doc->addStyleSheet($this->baseurl.'/templates/system/css/system.css');
//$doc->addStyleSheet($this->baseurl.'/templates/'.$this->template.'/css/reset.css', $type = 'text/css');
$doc->addStyleSheet($this->baseurl.'/templates/'.$this->template.'/css/style.css', $type = 'text/css');
$doc->addStyleSheet($this->baseurl.'/templates/'.$this->template.'/css/scroll.css', $type = 'text/css');
$doc->addStyleSheet($this->baseurl.'/templates/'.$this->template.'/css/tinyscrollbar.css', $type = 'text/css');

//add scripts
$doc->addScript('http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js', 'text/javascript');


$doc->addScript($this->baseurl.'/templates/'.$this->template.'/js/jquery.mousewheel.min.js', 'text/javascript');
$doc->addScript($this->baseurl.'/templates/'.$this->template.'/js/jquery.tinyscrollbar.min.js', 'text/javascript');
$doc->addScript($this->baseurl.'/templates/'.$this->template.'/js/jqueryrotate.2.1.js', 'text/javascript');
$doc->addScript($this->baseurl.'/templates/'.$this->template.'/js/cloud-carousel.1.0.5.min.js', 'text/javascript');

$doc->addScript($this->baseurl.'/templates/'.$this->template.'/js/scripts.js', 'text/javascript');

$doc->addScript($this->baseurl.'/templates/'.$this->template.'/js/content-scroll.js', 'text/javascript');


unset($this->_scripts[JURI::root(true).'/media/system/js/caption.js']);
unset($this->_scripts[JURI::root(true).'/media/system/js/core.js']);
unset($this->_scripts[JURI::root(true).'/media/system/js/mootools-core.js']);
unset($this->_scripts[JURI::root(true).'/media/system/js/mootools-more.js']);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" >
<head>
<jdoc:include type="head" />
<script type="text/javascript">
  //<![CDATA[
    $(window).load(function() { // makes sure the whole site is loaded
      $("#status").fadeOut("fast"); // will first fade out the loading animation
      $("#preloader").delay(0).fadeOut("fast"); // will fade out the white DIV that covers the website.
    })
  //]]>
</script>
</head>
<body class="<?php echo $this->language; ?>">

<div id="preloader">
	<div id="status">&nbsp;</div>
</div>

	<div class="main-header">
			
			<div class="line-1">
			
				<div class="main-logo">
				
					<a href="<?php echo $this->baseurl; ?>">Дебет-медиация</a>
				
				</div>
				
				<div class="lang-selector">
					<jdoc:include type="modules" name="lang" />
					<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="40" height="40" id="" align="middle">
						<param name="movie" value="images/1421044_flag_60x60.swf" />
						<param name="quality" value="high" />
						<param name="bgcolor" value="#000000" />
						<param name="play" value="true" />
						<param name="loop" value="true" />
						<param name="wmode" value="transparent" />
						<param name="menu" value="true" />
						<param name="devicefont" value="false" />
						<param name="salign" value="" />
						<param name="allowScriptAccess" value="sameDomain" />
					 
						<!--[if !IE]>-->
						<object type="application/x-shockwave-flash" data="images/1421044_flag_60x60.swf" width="40" height="40" id="">
							<param name="movie" value="images/1421044_flag_60x60.swf" />
							<param name="quality" value="high" />
							<param name="bgcolor" value="#000000" />
							<param name="play" value="true" />
							<param name="loop" value="true" />
							<param name="wmode" value="transparent" />
							<param name="menu" value="true" />
							<param name="devicefont" value="false" />
							<param name="salign" value="" />
							<param name="allowScriptAccess" value="sameDomain" />
					   
						<!--<![endif]-->
							<a href="http://www.adobe.com/go/getflash">
								<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
							</a>
						<!--[if !IE]>-->
						</object>
						<!--<![endif]-->
					</object>
				</div>
				
				<div class="clear"></div>
			
			</div>
			
			<div class="line-2">
			
				<div class="bot-logo"></div>
				
				<div class="mainmenu">
					<jdoc:include type="modules" name="mainmenu" />
				</div>
				
				<div class="phones">
				
				</div>
				
				<div class="clear"></div>
			
			</div>
			
		</div>
		
		<div id="page" class="content-wrapper" <?php if (($itemid != '107') and ($itemid != '108')) { ?>style="display:block;"<?php } else {?>style="display:none;"<?php } ?>>
		
			<div class="bg">
		
				<div id="main-content">
					<div id="close-btn"><a href="<?php echo $this->baseurl; ?>" title="На главную страницу">На главную страницу</a></div>
						<div class="vscroll">
						<jdoc:include type="component" />
						</div>
				</div>
			
			</div>
		
		</div>
		
		<div id="scrollbarX">
        
			<div class="scrollbar x_axis" style="width: 1841px;">
				<div class="scroll_left"></div>
				<div class="scroll_right"></div>
				<div class="track x_axis" style="width: 1841px;">
					<div class="thumb x_axis" style="left: 0px; width: 973.9313218390805px;">
						<div class="end x_axis"></div>
					</div>
				</div>
			</div>    
			
			<div class="viewport x_axis">
				<div id="container" class="overview x_axis">            
					<!-- ROOM 1 -->
					<div id="room1" class="box">
						<!-- Часы -->
						<div class="clock">
							<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="209" height="687" id="clock" align="middle">
					<param name="movie" value="/images/clock.swf">
					<param name="quality" value="high">
					<param name="bgcolor" value="#000000">
					<param name="play" value="true">
					<param name="loop" value="true">
					<param name="wmode" value="transparent">
					<param name="scale" value="showall">
					<param name="menu" value="false">
					<param name="devicefont" value="false">
					<param name="salign" value="">
					<param name="allowScriptAccess" value="sameDomain">
					<!--[if !IE]>-->
					<object type="application/x-shockwave-flash" data="/images/clock.swf" width="209" height="687">
						<param name="movie" value="images/clock.swf">
						<param name="quality" value="high">
						<param name="bgcolor" value="#000000">
						<param name="play" value="true">
						<param name="loop" value="true">
						<param name="wmode" value="transparent">
						<param name="scale" value="showall">
						<param name="menu" value="false">
						<param name="devicefont" value="false">
						<param name="salign" value="">
						<param name="allowScriptAccess" value="sameDomain">
					<!--<![endif]-->
						<a href="http://www.adobe.com/go/getflash">
							<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player">
						</a>
					<!--[if !IE]>-->
					</object>
					<!--<![endif]-->
				</object>
						</div>
						<!-- Книга - лого -->
						<a class="srcroller" href="#room1" id="book_logo" title=""></a>
						<!-- Колокольчик -->
						<div id="bell">
							<div id="bell_form" style="display:none;">
							<jdoc:include type="modules" name="callback" />
							</div>
						</div>
						<!-- Монетка -->
						<a class="scroller" href="#room1" id="monet" title=""></a>
						<!-- Книга -->
						<div id="book">
							<a href="/regulation" title="">
							<div class="tip" style="display: none;">Статьи и нормативные акты</div></a>
						</div>
						<!-- Картина - лого -->
						<a class="srcroller" href="#room1" id="picture" title=""></a>
						<!-- Часы с медведем -->
						<div id="bear"></div>
						<!-- Флаг -->
						<div id="flag"></div>
					</div>
					<!-- ROOM 2 -->
					<div id="room2" class="box">
					
						<div class="library-main">
							<jdoc:include type="modules" name="library" />
						</div>
					
						

		<!-- Карусель -->
		<!-- This is the container for the carousel. -->
		<div id="carousel1" style="position: relative; overflow: hidden;"><div style="position: absolute; width: 100%; height: 100%; display: block;">            
			<!-- All images with class of "cloudcarousel" will be turned into carousel items -->
			<!-- You can place links around these images -->
			 
					<img onclick="window.open('http://www.mediators.ru');" width="90" height="90" class="cloudcarousel" src="storage/ytp/img17.jpg" alt="" title="mediators.ru" >
			   
			 
					<img onclick="window.open('http://www.ebaoxford.co.uk/');" width="90" height="90" class="cloudcarousel" src="storage/ytp/img16.png" alt="" title="ebaoxford.co.uk" >
			   
			 
					<img onclick="window.open('http://www.dolgfactor.ru/');" width="90" height="90" class="cloudcarousel" src="storage/ytp/img15.png" alt="" title="dolgfactor.ru" >
			   
			 
					<img onclick="window.open('http://www.ogrn.ru/');" width="90" height="90" class="cloudcarousel" src="storage/ytp/img14.png" alt="" title="ogrn.ru" >
			   
			 
					<img onclick="window.open('http://www.zahvat.ru/');" width="90" height="90" class="cloudcarousel" src="storage/ytp/img13.png" alt="" title="zahvat.ru" >
			   
			 
					<img onclick="window.open('http://www.ips-safety.com/');" width="90" height="90" class="cloudcarousel" src="storage/ytp/img12.jpg" alt="" title="ips-safety.com" >
			   
			 
					<img onclick="window.open('http://www.deloros.ru/');" width="90" height="90" class="cloudcarousel" src="storage/ytp/img11.png" alt="" title="deloros.ru" >
			   
			 
					<img onclick="window.open('http://www.mgodeloros.ru/');" width="90" height="90" class="cloudcarousel" src="storage/ytp/img10.png" alt="" title="mgodeloros.ru" >
			   
			 
					<img onclick="window.open('http://news.peredsudom.ru/');" width="90" height="90" class="cloudcarousel" src="storage/ytp/img9.png" alt="" title="news.peredsudom.ru" >
			   
			 
					<img onclick="window.open('https://bsgcons.com/');" width="90" height="90" class="cloudcarousel" src="storage/ytp/img8.jpg" alt="" title="bsgcons.com" >
			   
			 
					<img onclick="window.open('http://corpcollection.ru/');" width="90" height="90" class="cloudcarousel" src="storage/ytp/img7.png" alt="" title="corpcollection.ru" >
			   
			 
					<img onclick="window.open('http://corpcoll.ru/');" width="90" height="90" class="cloudcarousel" src="storage/ytp/img6.png" alt="" title="corpcoll.ru" >
			   
			 
					<img onclick="window.open('http://www.prodolgi.com/');" width="90" height="90" class="cloudcarousel" src="storage/ytp/img5.png" alt="" title="prodolgi.com" >
			   
			 
					<img onclick="window.open('http://www.mediacia.com/');" width="90" height="90" class="cloudcarousel" src="storage/ytp/img3.png" alt="" title="mediacia.com" >
			   
			 
					<img onclick="window.open('http://www.collectori.ru/');" width="90" height="90" class="cloudcarousel" src="storage/ytp/img1.jpg" alt="" title="collectori.ru" >
			   
			 
					<img onclick="window.open('http://npnom.ru/');" width="90" height="90" class="cloudcarousel" src="storage/ytp/img2.jpg" alt="" title="npnom.ru" >
			   
								  
		</div></div>
							
		<!-- Define left and right buttons. -->
		<div id="left-but" style="display: inline;"></div>
		<div id="right-but" style="display: inline;"></div>
			
						
						<!-- Книга - ссылка -->
						<div id="book_links">
							<a target="_blank" href="http://www.obd-memorial.ru/html/about.htm" border="0"><img src="/images/photo_memor.png" border="0"></a>
						</div>
						
						<!--<div id="book_login">
							<div class="tip" style="display: none;"><a href="" title=""><strong>Войти</strong></a>&nbsp;<span class="small">или</span>&nbsp;<a href="" title=""><strong>Авторизироваться</strong></a></div>
						</div>-->
						<!-- Свитки -->
						<div id="svitok1" class="svitok"><a target="_blank" href="http://kad.arbitr.ru/" class="opened"></a></div>
						<div id="svitok2" class="svitok"><a target="_blank" href="http://www.fssprus.ru/iss/ip/" class="opened"></a></div>
						<div id="svitok3" class="svitok"><a target="_blank" href="http://www.fedresurs.ru/Companies" class="opened"></a></div>
						<div id="scrolls1"></div>
						<div id="scrolls2"></div>
					</div>
					<!-- ROOM 3 -->
					<!--<div id="room3" class="box">                
												  
				</div>-->
			</div>        
		</div>
	</div>

<script>
    (function($){
        $(window).load(function(){
            $(".vscroll").mCustomScrollbar({
			advanced:{ 
				updateOnContentResize:true, 
				autoScrollOnFocus: false
				}
			});
			
        });
    })(jQuery);
</script>
<jdoc:include type="modules" name="debug" />
</body>
</html>