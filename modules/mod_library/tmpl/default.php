<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_footer
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;


foreach($links as $link){
  if ($link['title'] != '') : ?>
    <div class="customlink item-<?php echo $i; ?>">
     <a href="<?php echo $link['url']; ?>" <?php if (strpos($link['url'],'debtmediation.ru') != true) : ?>target="_blank"<?php endif; ?>>
      <div class="tip" style="display:none;"><?php echo $link['title']; ?></div>
     </a>
    </div>
    <?php endif; $i++;
}

?>
