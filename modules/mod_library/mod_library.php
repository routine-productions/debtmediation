<?php
   
// No direct access to this file
defined('_JEXEC') or die;

$app		= JFactory::getApplication();

$links = '';
for ($i = 1; $i <= 201; $i++) {
  $links[$i]['title'] = htmlspecialchars($params->get('link_' .$i. '_title'));
  $links[$i]['url'] = htmlspecialchars($params->get('link_' .$i. '_url'));
}
$i = 1;

require JModuleHelper::getLayoutPath('mod_library', $params->get('layout', 'default'));

?>

