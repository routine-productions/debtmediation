{include file='head.tpl'} 
        
   <!-- Контент -->
<div id="page">
    <div class="content">        
        <div id="page_closer"></div>
        <div id="scrollbarY">
            <div class="viewportY">
        		<div class="overviewY">  
                
        <div class="h2 page_title">Реквизиты компании</div>
        <div class="page_content">
<p>
Прежде, чем вы начнете писать свою биографию, убедитесь, что вы "провели инвентаризацию" вашего происхождения, достижений, целей, музыкальных задач, и не забывайте, для кого вы пишете свою биографию - для представителей A&R в рекорд-лейблах, для работников масс-медиа, для организаторов концертов или для своих потенциальных менеджеров. Все эти профессионалы музыкального бизнеса - занятые люди, которые постоянно имеют дело с множеством людей, которые чего-то от них хотят. Поэтому сделайте свою биографию информативной, оптимистичной, заполненной полезными комментариями, описаниями и цитатами. Ваша биография должна мотивировать профессионалов музыкальной индустрии послушать вашу музыку и помочь вам на вашем музыкальном пути. Приготовьтесь к рок-н-роллу и сделайте свою биографию организованной, короткой и сосредоточенной.
</p>
<div class="h2">
Заголовок второго уровня
</div>
<p>
Начните с вводного предложения, в котором ясно указывается название группы/имя артиста, жанр вашей музыки, откуда вы, дословный положительный отзыв какого-нибудь профессионала музыкального бизнеса о вашей музыке (если такой отзыв есть).
</p>
<div class="h2">
2-й абзац
</div>
<p>
В этом абзаце нужно описать непосредственную цель биографии. Чем вы занимаетесь в настоящее время? Упомяните свою текущую деятельность. Если у вас выходит новый компакт-диск или цифровой релиз, то это должно быть главной мыслью первого предложения второго абзаца вашей биографии. Другими словами, вы сразу должны пояснить причину написания своей биографии. Упоминания о любых промо-действиях, которые вы точно будете предпринимать в поддержку своего CD, также полезно вставить во второй абзац.
</p>
                    </div>
                </div>
            </div>
            <div class="scrollbarY">
                <div class="trackY">
                    <div class="thumbY"><div class="endY"></div></div>
                </div>
            </div>
        </div>
    </div> 
</div>     
    <div id="menu">
        <div id="text"></div>
        <div id="contacts">
            <span class="tel">(495) <strong>505 30 13</strong></span><br />
            <span>Время работы: Пн-Пт 9 до 17</span>
        </div>
        {$menu}
		<!--<table>
            <tr>
    			<td>
                    <a href="#room1" title="">О компании</a>
                    <div class="submenu">
                        <ul>
                            <li><a href="" title="">Руководство</a></li>
                            <li><a href="" title="">Реквизиты</a></li>
                            <li><a href="" title="">Услуги&nbsp;и&nbsp;цены</a></li>
                            <li><a href="" title="">Политика&nbsp;конфиденциальности</a></li>
                            <li><a href="" title="">Продажа&nbsp;долгов</a></li>
                        </ul>
                    </div>
                </td>
    			<td><a href="#room2" title="">Партнеры</a></td>
    			<td><a href="#room3" title="">Новости</a></td>
                <td><a href="#room3" title="">Контакты</a></td>
            </tr>
		</table>  -->      
	</div>
			
	<div id="scrollbarX">
        
        <div class="scrollbar x_axis">
            <div class="scroll_left"></div>
            <div class="scroll_right"></div>
            <div class="track x_axis">
                <div class="thumb x_axis">
                    <div class="end x_axis"></div>
                </div>
            </div>
        </div>    
		
        <div class="viewport x_axis">
    		<div id="container" class="overview x_axis">            
                <!-- ROOM 1 -->
        		<div id="room1" class="box">
                    <!-- Часы -->
                    <div class="clock">
                        <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="209" height="687" id="clock" align="middle">
				<param name="movie" value="images/clock.swf" />
				<param name="quality" value="high" />
				<param name="bgcolor" value="#000000" />
				<param name="play" value="true" />
				<param name="loop" value="true" />
				<param name="wmode" value="transparent" />
				<param name="scale" value="showall" />
				<param name="menu" value="false" />
				<param name="devicefont" value="false" />
				<param name="salign" value="" />
				<param name="allowScriptAccess" value="sameDomain" />
				<!--[if !IE]>-->
				<object type="application/x-shockwave-flash" data="images/clock.swf" width="209" height="687">
					<param name="movie" value="images/clock.swf" />
					<param name="quality" value="high" />
					<param name="bgcolor" value="#000000" />
					<param name="play" value="true" />
					<param name="loop" value="true" />
					<param name="wmode" value="transparent" />
					<param name="scale" value="showall" />
					<param name="menu" value="false" />
					<param name="devicefont" value="false" />
					<param name="salign" value="" />
					<param name="allowScriptAccess" value="sameDomain" />
				<!--<![endif]-->
					<a href="http://www.adobe.com/go/getflash">
						<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
					</a>
				<!--[if !IE]>-->
				</object>
				<!--<![endif]-->
			</object>
                    </div>
                    <!-- Книга - лого -->
                    <a class="srcroller" href="#room1" id="book_logo" title=""></a>
                    <!-- Колокольчик -->
                    <div id="bell">
                        <div id="bell_form">
                            <div class="bform_title">Вы можете заказать обратный звонок и
 мы перезвоним вам  в удобное для вас время.</div>
                            <form action="">
                                <input type="text" data-value="Ваше имя..." value="Ваше имя..." name="" />
                                <input type="text" data-value="Ваш телефон..." value="Ваш телефон..." name="" />
                                <textarea name="" data-value="Написать удобное время для звонка..." rows="" cols="">Написать удобное время для звонка...</textarea>
                                <div class="button">Отправить</div>
                            </form>
                        </div>
                    </div>
                    <!-- Монетка -->
                    <a class="scroller" href="#room1" id="monet" title=""></a>
                    <!-- Книга -->
                    <div id="book">
                        <a onclick="open_content('pages','Статьи и нормативные акты',84);" class="scrollTo scroller" href="#room3" title=""></a>
                        <div class="tip">Статьи и нормативные акты</div>
                    </div>
                    <!-- Картина - лого -->
                    <a class="srcroller" href="#room1" id="picture" title=""></a>
                    <!-- Часы с медведем -->
                    <div id="bear"></div>
                    <!-- Флаг -->
                    <div id="flag"></div>
        		</div>
        		<!-- ROOM 2 -->
        		<div id="room2" class="box">
                
                    {if isset($slider)}{$slider}{/if}
                    
                    <!-- Книга - авторизация -->
                    <div id="book_login">
                        <div class="tip"><a href="" title=""><strong>Войти</strong></a>&nbsp;<span class="small">или</span>&nbsp;<a href="" title=""><strong>Авторизироваться</strong></a></div>
                    </div>
                    <!-- Свитки -->
                    <div id="svitok1" class="svitok"><a target="_blank" href="http://kad.arbitr.ru/" class="opened"></a></div>
                    <div id="svitok2" class="svitok"><a target="_blank" href="http://www.fssprus.ru/iss/ip/" class="opened"></a></div>
                    <div id="scrolls1"></div>
                    <div id="scrolls2"></div>
        		</div>
                <!-- ROOM 3 -->
        		<!--<div id="room3" class="box">                
                                              
            </div>-->
        </div>        
	</div>
</div>
        
{include file='footer.tpl'}